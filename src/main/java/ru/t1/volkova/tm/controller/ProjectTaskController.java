package ru.t1.volkova.tm.controller;

import ru.t1.volkova.tm.api.controller.IProjectTaskController;
import ru.t1.volkova.tm.api.service.IProjectTaskService;
import ru.t1.volkova.tm.util.TerminalUtil;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(final IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("[ENTER PROJECT ID]");
        final String projectID = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID]");
        final String taskID = TerminalUtil.nextLine();
        projectTaskService.bindTaskToProject(projectID, taskID);
    }

    @Override
    public void unbindTaskFromProject() {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("[ENTER PROJECT ID]");
        final String projectID = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID]");
        final String taskID = TerminalUtil.nextLine();
        projectTaskService.unbindTaskFromProject(projectID, taskID);
    }

}
