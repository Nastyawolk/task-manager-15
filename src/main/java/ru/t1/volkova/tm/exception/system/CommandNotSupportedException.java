package ru.t1.volkova.tm.exception.system;

public final class CommandNotSupportedException extends AbstractException {

    public CommandNotSupportedException() {
        super("Error! This command not supported...");
    }

    public CommandNotSupportedException(final String command) {
        super("Error! Command \"" + command + "\" not supported...");
    }

}
