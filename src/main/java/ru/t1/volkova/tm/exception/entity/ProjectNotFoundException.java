package ru.t1.volkova.tm.exception.entity;

public final class ProjectNotFoundException extends AbstractEntityNotFoundException{

    public ProjectNotFoundException() {
        super("Error! Project Not found...");
    }

}
