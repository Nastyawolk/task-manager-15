package ru.t1.volkova.tm.exception.system;

public final class ArgumentNotSupportedException extends AbstractException {

    public ArgumentNotSupportedException() {
        super("Error! This argument not supported...");
    }

    public ArgumentNotSupportedException(final String argument) {
        super("Error! Argument \"" + argument + "\" not supported...");
    }

}
