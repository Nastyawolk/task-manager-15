package ru.t1.volkova.tm.api.controller;

public interface ICommandController {

    void showInfo();

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

}
