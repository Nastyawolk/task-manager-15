package ru.t1.volkova.tm.model;

public final class Command {

    private String name;

    private String argument;

    private String desription;

    public Command() {
    }

    public Command(final String name, final String argument, final String desription) {
        this.name = name;
        this.argument = argument;
        this.desription = desription;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(final String argument) {
        this.argument = argument;
    }

    public String getDesription() {
        return desription;
    }

    public void setDesription(final String desription) {
        this.desription = desription;
    }

    @Override
    public String toString() {
        String displayName = "";
        if (name != null && !name.isEmpty()) displayName += name;
        if (argument != null && !argument.isEmpty()) displayName += ", " + argument;
        if (desription != null && !desription.isEmpty()) displayName += ": " + desription;
        return displayName;
    }

}
